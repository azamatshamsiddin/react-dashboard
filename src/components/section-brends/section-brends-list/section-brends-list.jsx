import React from 'react';

const SectionBrendsList = ({data}) => {
  return (
    <ul className="flex items-center justify-between flex-wrap py-7">
      {
        data.map(({id, brendImgUrl, brendName}) => (
          <li className="w-44 mb-5" key={id}>
            <a href="src/components/section-brends/section-brends-list/section-brends-list#"
               className="flex flex-col items-center justify-between min-w-36 lg:w-auto h-24 p-4 overflow-hidden bg-white border bg-opacity-70 rounded-2xl group relative">
              <div className="w-15 h-10 flex items-center justify-center">
                <img
                  src={brendImgUrl} width="60"
                  alt={brendName}
                  className="mb-1 transform scale-75 group-hover:scale-100 transition duration-500 ease-in-out max-w-full"/>
              </div>
              <p className="capitalize text-center text-base mt-2">
                {brendName}
              </p>
              <div
                className="absolute top-0 left-0 bg-gray-400 w-full h-full opacity-0 duration-500 group-hover:opacity-20">
              </div>
            </a>
          </li>
        ))
      }
    </ul>
  );
};

export default SectionBrendsList;
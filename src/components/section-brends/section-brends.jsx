import React from 'react';
import SectionTitle from "../UI/section-title/section-title";
import SectionBrendsList from "./section-brends-list/section-brends-list";

const SectionBrends = (props) => {
  const {data, title}= props
  return (
    <section className="container mx-auto px-2 mt-14">
      <SectionTitle title={title}/>
      <SectionBrendsList data={data}/>
    </section>
  );
};

export default SectionBrends;
import React  from 'react';
import { GiHamburgerMenu } from "react-icons/gi";

const CatalogBtn = ({ sticky, openModal}) => {
  return (
    <button onClick={openModal}
            className={(sticky ? `border-black text-black hover:bg-red-600 hover:text-white ` : ` hover:bg-red-50 hover:text-red-500 hover:border-red-500  border-white`) + " " + `sm:rounded-xl md:rounded-full sm:px-2.5 py-2.5 px-4 flex items-center border-solid border-2 transition ease-in-out duration-300 mr-4`}>
      <span className="sm:hidden md:block mr-2">Kataloglar</span><GiHamburgerMenu size="20px"/>
    </button>
  );
};

export default CatalogBtn;
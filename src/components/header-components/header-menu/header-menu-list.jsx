import React from 'react';
import { FiHeart, FiUser } from "react-icons/fi";
import HeaderMenuItem from "./header-menu-item";
import { RiShoppingBag3Line } from "react-icons/ri";
import { BiBarChart } from "react-icons/bi"


const menuItems = [
  {
    id: 1,
    title: "Kirish",
    icon: <FiUser size="25px" className="mb-2"/>,
  },
  {
    id: 2,
    title: "Taqqoslash",
    icon: <BiBarChart size="25px" className="mb-2"/>,
  },
  {
    id: 3,
    title: "Sevimlilar",
    icon: <FiHeart size="25px" className="mb-2"/>,
  },
  {
    id: 4,
    title: "Savatcha",
    icon: <RiShoppingBag3Line size="25px" className="mb-2"/>,
  },
]

const HeaderMenuList = ({sticky, favorite}) => {
  return (
    <ul className="flex items-center ml-auto">
      {
        menuItems.map(({id, title, icon}) => (
          <HeaderMenuItem favorite={favorite} sticky={sticky}  key={id} title={title} icon={icon}/>
        ))
      }
    </ul>
  );
};

export default HeaderMenuList;
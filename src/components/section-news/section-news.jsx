import React from 'react';
import SectionTitle from "../UI/section-title/section-title";
import { Splide, SplideSlide, SplideTrack } from "@splidejs/react-splide";
import { MdOutlineRemoveRedEye } from "react-icons/md";

const SectionNews = (props) => {
  const {data, title} = props
  return (
    <section className="container mx-auto px-2 mt-14">
      <SectionTitle title={title} navigateTitle={"Hammasini o'qish"}/>
      <div className="mt-7">
        <Splide hasTrack={false} options={
          {
            rewind: true,
            waitForTransition: true,
            arrows: true,
            pagination: true,
            perPage: 3,
            width: "inherit",
            height: 300,
            type: 'loop',
            gap: '3rem',
            // autoplay: true,
            perMove: 1,
            padding: {
              left: 40,
              right: 40
            }
          }
        }>
          <SplideTrack>
            {
              data.map(({id, newsTitle, views, createdAt, description})=>(
                <SplideSlide key={id} className="w-64 ">
                  <a href="#" className="group flex flex-col items-start h-full">
                    <div className="flex items-center"><MdOutlineRemoveRedEye size={20} className="text-red-600"/> <span className="ml-4 font-bold">{views}</span></div>
                    <h4 className="group-hover:text-red-600 font-bold text-lg mt-5 mb-4">{newsTitle}</h4>
                    <p className="text-gray-700 font-thin text-md">{description}</p>
                    <p className="text-base font-bold ml-auto mt-auto">{createdAt}</p>
                  </a>
                </SplideSlide>
              ))
            }
        
          </SplideTrack>
        </Splide>
      </div>
  
    </section>
  );
};

export default SectionNews;
import React from 'react';

const Loader = () => {
  
  return (
    <div>
      <h1 className="text-xl text-red-500 font-bold">Loading...</h1>
    </div>
  )
    ;
};

export default Loader;
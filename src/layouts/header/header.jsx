import React, { useState } from 'react';

import CatalogBtn from "../../components/header-components/header-catalog-btn";
import SearchBar from "../../components/header-components/header-search-bar";
import HeaderLogo from "../../components/header-components/header-logo";
import HeaderMenuList from "../../components/header-components/header-menu/header-menu-list";


const Header = ({openModal, favorite}) => {
  const [sticky, setSticky] = useState(false)
  
  window.addEventListener("scroll", () => {
    if (window.scrollY > 120) {
      return setSticky(true)
    }
    return setSticky(false)
  })
  
  return (
    <header className={sticky ? `bg-white py-4 max-w-full sticky top-0 z-30 shadow-xl transition-all ease-in duration-200` : `bg-gradient py-5 max-w-full`}>
      <div className="container mx-auto px-2 sm:px-4 md:px-2 text-white flex items-center">
        <HeaderLogo sticky={sticky}/>
        <CatalogBtn openModal={openModal} sticky={sticky}/>
        <SearchBar sticky={sticky}/>
        <HeaderMenuList favorite={favorite} sticky={sticky}/>
      </div>
    </header>
  );
};

export default Header;
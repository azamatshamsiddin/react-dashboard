import React from 'react';
import '@splidejs/splide/css';
import { Splide, SplideSlide, SplideTrack } from "@splidejs/react-splide";

const data = [
  {
    id: 1,
    imgUrl: "https://olcha.uz/image/60x50/category/M90cCGAT8ARmlnxJzt5sH4cTD4eBUjWocRW36j69zghIlMA6leRkjL9mvoBr.png",
    title: "Smartfonlar, gadjet, aksessuarlar"
  },
  {
    id: 2,
    imgUrl: "https://olcha.uz/image/60x50/category/LMk7YuQAzKqsUIBDfr4jRA2IEFsLPsyFkWAWFHYkwlQZ8WRVvWut5Heb8Dju.png",
    title: "Noutbuk, printer, kompyuter"
  },
  {
    id: 3,
    imgUrl: "https://olcha.uz/image/60x50/category/6zDy6H7NAKLmaIszRCYFLxiXDtZDvNapSNOmLkLZJUpuXpRTtEq98OC9gX5J.png",
    title: "Televizor"
  },
  {
    id: 4,
    imgUrl: "https://olcha.uz/image/60x50/category/FwbLWc8SYnihYCcCTQzGbOCBgowRWJrt8SHh3pWKaVp8YwCfvs48I6Hc9oms.",
    title: "Mebel"
  },
  {
    id: 5,
    imgUrl: "https://olcha.uz/image/60x50/category/uSZ7ukWOhOGAavEsU53jITihn8GZwiqeCmnzaErWodbkagi99gq739isSfC2.png",
    title: "Geymerlar uchun"
  },
  {
    id: 6,
    imgUrl: "https://olcha.uz/image/60x50/category/gTEdspBADkaKoLWlt6Tyc1XrGeJAiyfd8zwwP52GBOFDtAVnVtj4jDOwbCMz.png",
    title: "Maishiy texnika"
  },
  {
    id: 7,
    imgUrl: "https://olcha.uz/image/60x50/category/3OqvliYxYnrZoZKX7ZSuIyAPCC3C4lp11u45G04T8RbYWan6GDrMBB0gwre1.png",
    title: "Barchasi oshxona uchun"
  },
  {
    id: 8,
    imgUrl: "https://olcha.uz/image/60x50/category/odtXyERULXCcAjKtJEb0kH7XBV0InZmmqAwRqk24I1zqtITNExpsa7wto6uN.png",
    title: "Kitoblar"
  },
  {
    id: 9,
    imgUrl: "https://olcha.uz/image/60x50/category/oACT4XAZ4raQLa0WSGelk0qgRnRurDnY8jpZaQiu5ftyAGh8LQyctUOCLbRf.",
    title: "Barchasi uy uchun"
  },
  {
    id: 10,
    imgUrl: "https://olcha.uz/image/60x50/category/ppPdVKboDyJeSIwPV2azbrUGGWU1NmeBICAEpufF5ZriTQsHT6MvCQKpDyUC.png",
    title: "Qurilish, ta'mirlash"
  },
  {
    id: 11,
    imgUrl: "https://olcha.uz/image/60x50/category/opA1z5cMyRtEZW6Oa02AXZnH0l74jiTNDA10QXTKZXS8fsoYNuPAmjID5xUT.png",
    title: "Sport anjomlari"
  },
  {
    id: 12,
    imgUrl: "https://olcha.uz/image/60x50/category/8AHSgv20VYFMvDFCykQOb25FsT2zXTroCgxaNugsZmfaA1fHbFKswZT9tFqq.png",
    title: "Avto jihozlar"
  },

]


const SplideSlider = () => {
  return (
    <section className={"products-list container px-2 my-4 mx-auto text-black"}>
      <Splide options={
        {
          rewind: true,
          waitForTransition: true,
          arrows: true,
          wheel: true,
          pagination: false,
          perPage: 8,
          width: "inherit",
          height: 140,
          // type: 'loop',
          gap: '1rem',
          // autoplay: true,
          perMove: 1,
          padding: {
            left: 40,
            right: 40
          }
        }
      } hasTrack={false} aria-label="Prroducts">
        <SplideTrack className="px-3 w-full">
          {
            data.map(item => (
              <SplideSlide key={item.id} className="flex items-start justify-center">
                <a className="pt-3 px-2 flex flex-col items-center text-center w-full" href="#">
                  <img className={"mb-2.5"} src={item.imgUrl} alt={item.title}/>
                  <h4 className={"text-base"}>{item.title}</h4>
                </a>
              </SplideSlide>
            ))
          }
        </SplideTrack>
      </Splide>
    
    </section>
  
  
  )
    ;
};


export default SplideSlider;
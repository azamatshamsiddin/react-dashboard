import React from 'react';
import LogoBrendRed from "../../assets/logo-red.png";
import { FaFacebookF, FaInstagram, FaTelegramPlane } from "react-icons/fa";

const Footer = () => {
  const currentYear = new Date().getFullYear()
  return (
    <footer className="footer-bg text-white mt-28">
      <div className="container mx-auto px-2 ">
        <div className="py-14 flex items-start justify-between">
          <div className="footer-left">
            <div className="footer-left__text flex">
              <div>
                <img className="footer-logo " src={LogoBrendRed} alt="logo-brend"/>
                <p className="mt-6 mb-2 text-base">Call-markazi:</p>
                <a className="hover:text-red-600 text-md" href="tel:+998712022021">+998 (71) 202 202 1</a>
              </div>
              <ul className="ml-16">
                <li className="group "><a className=" flex items-center group-hover:text-red-600 text-base font-thin"
                                          href="#"><FaFacebookF
                  className="mr-2 text-black bg-white p-1.5 rounded-full w-8 h-8 group-hover:text-red-600"/>Facebook</a>
                </li>
                <li className="group my-5"><a className=" flex items-center hover:text-red-600 text-base font-thin"
                                              href="#"><FaInstagram
                  className="mr-2 text-black bg-white p-1.5 rounded-full w-8 h-8 group-hover:text-red-600"/>Instagram</a>
                </li>
                <li className="group "><a className=" flex items-center hover:text-red-600 text-base font-thin"
                                          href="#"><FaTelegramPlane
                  className="mr-2 text-black bg-white p-1.5 rounded-full w-8 h-8 group-hover:text-red-600"/>Telegram</a>
                </li>
              </ul>
            </div>
            
            <div>
              <ul className="flex items-center mt-8">
                <li className="mr-3">
                  <a href=""><img src="https://olcha.uz/_nuxt/img/appstore.367673c.png" alt="app-store"
                                  width="120"/></a>
                </li>
                <li className="mr-3">
                  <a href=""><img src="https://olcha.uz/_nuxt/img/googleplay.c20cdde.png" alt="app-store" width="120"/></a>
                </li>
                <li>
                  <a href=""><img src="https://olcha.uz/_nuxt/img/app-gallery.9b99158.svg" alt="app-store" width="120"/></a>
                </li>
              </ul>
            </div>
          </div>
          
          {/*footer center*/}
          <div className="footer-center ml-24">
            <h4 className="text-red-600 text-lg font-bold mb-6">Ma'lumot</h4>
            <ul className="col columns-2">
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Biz haqimizda</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Bo'sh ish o'rinlari</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Ommaviy oferta</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">To'lovni qaytarish va almashtirish</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Muddatli to'lov shartlari</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Onlayn to'lash</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Eco-Friendly</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Shaxsiy ma'lumotlar bilan ishlash bo'yicha
                  siyosat</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">To'lov va yetkazib berish</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Bonus va aksiyalar</a>
              </li>
              <li className="mb-3">
                <a className="hover:text-gray-400 text-base " href="#">Servis markazlar</a>
              </li>
            </ul>
            <div className="mt-8">
              <h4 className="text-red-600 text-lg font-bold mb-6">Biz haqimizda</h4>
              
              <ul className="col columns-2">
                
                <li className="mb-3">
                  <a className="hover:text-gray-400 text-base " href="#">Fikr qoldiring</a>
                </li>
                <li className="mb-3">
                  <a className="hover:text-gray-400 text-base " href="#">Manzil</a>
                </li>
                <li className="mb-3">
                  <a className="hover:text-gray-400 text-base " href="#">Kontaktlar</a>
                </li>
                <li className="mb-3">
                  <a className="hover:text-gray-400 text-base " href="mailto:info@olcha.uz">info@olcha.uz</a>
                </li>
              
              </ul>
            </div>
          
          </div>
          <div className="footer-right">
            <ul className="grid grid-cols-3 gap-5">
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35} src="https://olcha.uz/_nuxt/img/apelsin.93106ae.png" alt=""/>
                </a>
              </li>
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35}
                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAAAjCAMAAADrNwfvAAAArlBMVEUAAAAAPYcAPocAPYcAO4UAOYUANYYAPocAPIYAPYcAP4YAPIYAP4gAPocAPogAPocAPoUAQnYAOoUAPoYAPYcAPYYAPocAPYYAPocAPocAPYcAPocAPocAPYcAPocAPoUAPocAPYcAPoYAPokAPYYAPocAPocAPYYAPocAPYcAPogAPocAPYcAPof/hgD/gAAAPYgAPYYAPYf/iQD/hwD/igAAO4T/gAAAPof/iAAXwlIsAAAAOHRSTlMA/fmHGikJwSXzeFmnk3TrIAMWgmUP5bh9793Ww2lLQNCccDk0y4xFq2BSLqOVaQa7s5OG5MQeEsvbhOMAAAIkSURBVEjH3ZRpz6IwEIDbUgtF5RBEFA8Q72N9jz3G///HFmZqSMy7yQb5sNnnA5kp7UNnKLA/YQEyoMz25ef9fv/G2vDsguj2ozPXtPz1+b0jF+hY3W7duCryouzGhSz+Z1ffQ85duEYSUf9aja+5IkAOlM0pO7ZzuYDMMVEZZad2riMgPrbb0ZS1fI+lAMSrk5Rirdq51AwQYW02lvFuWUuuYBAcDGlbVyDgCX/E2pI+qXiftUbtwGAax17hOGtU2Yq9Rngq8qnWib3bh6wDQvU3mvJjKFk3jGbAe0z1al+IVzbuIWYj4/0STwHdcWiU5oyNogqDelz64EsWcLuKHT5hFWuOOLhmqAHEVdEqi/MlRX1eIS5odus4sVQ1+tZnLIB549p4nvdGZzK8QL6I5xDR+UiEOBgXTLw0gzW6YODtcnhH8ZOrjhLawF5ksipiTVUuocj0yLiiOr+QK67m5LD60qVs+MDAoycT4USsLFgb19U5F5AaF/6eoi9dEdjKfEFx4zrrXDliq8glhOAzp3GdYPdwbetGwTvVon2HxmP6PZPYhW0UTcWGXLalfckal/V47HjG++NgABaKM+5KKWlcuKPxwo9r4ZbrCige/bIgNS5X9lLhB4yIhZj5kGFj1wDTirKOVxqShPu9OqxKcpySJ8q4ZOL/JJeeCkiaT3VV2BOP9rwfIg5V707stIdbGZ5w5TCo2zFc1m09hGb+ri8Z8RuJv2K4FAf4iAAAAABJRU5ErkJggg=="
                       alt=""/>
                </a>
              </li>
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35} src="https://olcha.uz/_nuxt/img/payme.e57aa2a.png" alt=""/>
                </a>
              </li>
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35} src="https://olcha.uz/_nuxt/img/click.93445f6.png" alt=""/>
                </a>
              </li>
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35} src="https://olcha.uz/_nuxt/img/humo.7dc7e40.jpeg" alt=""/>
                </a>
              </li>
              <li className="">
                <a
                  className="flex items-center justify-center bg-white text-center rounded-md w-16 h-16 overflow-hidden"
                  href="">
                  <img width={35} src="https://olcha.uz/_nuxt/img/logo-paynet.48b4b59.png" alt=""/>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="border-t border-black py-8">
        <div className="container mx-auto px-2 flex items-center">
          <p className="text-base font-thin text-gray-400">&copy; 2017-{currentYear}. ООО "Olcha store-copy"</p>
          <div className="ml-auto">
            <a className="text-base text-gray-400 hover:text-white mr-3" href="#">Ommaviy oferta</a>
            <a className="text-base text-gray-400 hover:text-white" href="#">Maxfiylik siyosati</a>
          </div>
        </div>
      </div>
    
    </footer>
  );
};

export default Footer;